# PHP SCSS

PHP SCSS compiler.

## Installation

Add to `composer.json`, `repositories` section

```
  "repositories": [
    {
      "type": "package",
      "package": {
        "name": "testudio/phpscss",
        "version": "v0.0.1",
        "type": "package",
        "source": {
          "url": "git@gitlab.com:testudio/phpscss.git",
          "type": "git",
          "reference": "master"
        }
      }
    }
  ],
```

and then run

```
composer require testudio/phpscss
```

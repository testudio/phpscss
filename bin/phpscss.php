<?php
  require __DIR__ . '/../vendor/autoload.php';

  use Symfony\Component\Console\Application;
  use Symfony\Component\Console\Input\InputArgument;
  use Symfony\Component\Console\Input\InputInterface;
  use Symfony\Component\Console\Input\InputOption;
  use Symfony\Component\Console\Output\OutputInterface;

  (new Application('phpscss', '0.0.1'))
    ->register('phpscss')
    ->addArgument('file', InputArgument::OPTIONAL, 'SCSS file')
    ->setCode(function(InputInterface $input, OutputInterface $output) {
      // output arguments and options
    })
    ->getApplication()
    ->setDefaultCommand(Sass::compile('woot.scss'), true) // Single command application
    ->run();
